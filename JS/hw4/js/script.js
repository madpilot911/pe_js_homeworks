function createUser(firstName, lastName) {
    firstName = prompt("Enter first name", "First Name");
    lastName = prompt("Enter last name", "Last Name");
    const user = {};
    Object.defineProperties(user, {
        firstName: {
            value: firstName,
        },
        lastName: {
            value: lastName,
        },
    });
    return user;
}

const newUser = createUser();

newUser.getLogin = function() {
    return (this.firstName.charAt(0) + this.lastName).toLowerCase();
};

const login = newUser.getLogin();
console.log(login);

newUser.firstName = "Hui";
console.log(newUser);