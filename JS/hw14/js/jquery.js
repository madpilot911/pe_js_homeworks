const page = $("html");
const scrollBtn = $(
    `<button id="scroll-btn"><i class="fas fa-arrow-circle-up"></i></button>`
);
$("body").append(scrollBtn);
const headSection = $(".head-section");
const menu = $("<ul></ul>");
const topRatedSection = $("#top-rated");
const hideBtn = $(`<button class = "hide-btn">hide</button>`);
$("#hot-news").prepend(hideBtn);
const popularPosts = $(`<li><a href = "#popular-posts">popular posts</a><li>`);
const popularClients = $(
    `<li><a href = "#popular-clients">popular clients</a><li>`
);
const topRated = $(`<li><a href = "#top-rated">top rated</a><li>`);
const hotNews = $(`<li><a href = "#hot-news">hot news</a><li>`);
menu.addClass("inner-menu");
menu.append(popularPosts);
menu.append(popularClients);
menu.append(topRated);
menu.append(hotNews);

headSection.prepend(menu);
const listItems = $(".inner-menu a");
listItems.addClass("inner-menu__item");
console.log(listItems);

$(`a[href*="#"]`).on("click", function() {
    page.animate({
            scrollTop: $($.attr(this, "href")).offset().top,
        },
        400
    );
    return false;
});

$(window).on("scroll", function() {
    if ($(window).scrollTop() > $(window).height()) {
        scrollBtn.addClass("show");
    } else {
        scrollBtn.removeClass("show");
    }
});
scrollBtn.on("click", function(e) {
    e.preventDefault();
    page.animate({ scrollTop: 0 }, 400);
});

hideBtn.on("click", function(e) {
    topRatedSection.slideToggle();
    hideBtn.text() === "hide" ? hideBtn.text("show") : hideBtn.text("hide");
});