let limit = +prompt("Enter integer", 0);
let result;
while (!Number.isInteger(limit)) {
    limit = +prompt("Try again. Enter number without decimal point", 0);
}
for (let i = 1; i <= limit; i++) {
    if (i % 5 === 0) {
        result = i;
        console.log(result);
    }
}
if (!result) {
    result = "Sorry, no numbers";
}
console.log(result);