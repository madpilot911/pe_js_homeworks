let m = +prompt("Enter integer", 0);
let n = +prompt("Enter integer", 0);
while (!Number.isInteger(m) || !Number.isInteger(n)) {
    m = +prompt("Try again. Enter number without decimal point", 0);
    n = +prompt("Try again. Enter number without decimal point", 0);
}

if (m > n) {
    let temp = m;
    m = n;
    n = temp;
}

for (let i = m; i <= n; i++) {

    let result;

    for (let j = 2; j < i; j++) {

        if (i % j === 0) {
            result = true;
        }
    }

    if (!result && i > 1) {
        console.log(i);
    }
}