/**
 * Create new object "user"
 * @return {Object}
 */
function createUser() {
    firstName = prompt("Enter first name", "First Name");
    lastName = prompt("Enter last name", "Last Name");
    birthday = prompt("Enter date of birth", "dd.mm.yyyy");
    const user = {
        firstName,
        lastName,
        //Change date of birth format from "dd.mm.yyyy" to "yyyy-mm-dd"
        birthday: birthday.split(".").reverse().join("-"),
        getLogin: () => (this.firstName.charAt(0) + this.lastName).toLowerCase(),
        getAge: () =>
            Math.floor(
                (Date.now() - new Date(this.birthday)) / (365.25 * 24 * 3600 * 1000)
            ),
        getPassword: () =>
            this.firstName.charAt(0).toUpperCase() +
            this.lastName.toLowerCase() +
            new Date(newUser.birthday).getFullYear(),
    };

    return user;
}

const newUser = createUser();
const age = newUser.getAge();
const password = newUser.getPassword();

console.log(newUser);
console.log(age);
console.log(password);