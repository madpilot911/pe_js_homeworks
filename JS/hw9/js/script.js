const homeWorkTabs = {
    tabs: document.querySelector(".tabs"),
    tabsTitle: document.querySelectorAll(".tabs .tabs-title"),
    tabsContent: document.querySelector(".tabs-content").children,
    activateTab: function(e) {
        homeWorkTabs.tabsTitle.forEach((element) =>
            element.classList.remove("active")
        );
        e.target.classList.add("active");
    },
    activateContent: function() {
        for (let i = 0; i < homeWorkTabs.tabsContent.length; i++) {
            homeWorkTabs.tabsContent[i].dataset.hidden = "true";
        }
        for (let i = 0; i < homeWorkTabs.tabsTitle.length; i++) {
            if (homeWorkTabs.tabsTitle[i].classList.contains("active")) {
                homeWorkTabs.tabsContent[i].dataset.hidden = "false";
            }
        }
    },
};

homeWorkTabs.tabs.addEventListener("click", homeWorkTabs.activateTab);
homeWorkTabs.tabs.addEventListener("click", homeWorkTabs.activateContent);