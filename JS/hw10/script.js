const submitBtn = document.querySelector(".btn");

(function deleteWarningSpan() {
    const inputs = document.querySelectorAll("input");

    for (let i = 0; i < inputs.length; i++) {
        inputs[i].addEventListener("focus", (e) => {
            const span = document.querySelector("span");

            if (span) {
                span.remove();
            }
        });
    }
})();

submitBtn.addEventListener("click", (e) => {
    e.preventDefault();
    if (
        document.querySelector("#first-input").value ===
        document.querySelector("#second-input").value &&
        document.querySelector("#first-input").value
    ) {
        alert("You are welcome!");
    } else {
        const form = document.querySelector(".password-form");
        form.insertAdjacentHTML(
            "beforeend",
            "<span>Нужно ввести одинаковые значения</span>"
        );
    }
});

const showPassIcons = document.querySelectorAll(".icon-password");
showPassIcons.forEach((icon) =>
    icon.addEventListener("click", (e) => {
        if (e.target.previousElementSibling.type === "password") {
            e.target.previousElementSibling.type = "text";
            e.target.classList.remove("fa-eye-slash");
            e.target.classList.add("fa-eye");
            // эти игры с классами кажутся мне неоптимальными, но ничего другого не придумал
        } else {
            e.target.previousElementSibling.type = "password";
            e.target.classList.remove("fa-eye");
            e.target.classList.add("fa-eye-slash");
        }
    })
);