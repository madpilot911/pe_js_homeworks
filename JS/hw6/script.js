/**
 * Creating new array without specifed type of items
 * @param {Array} arr initial array
 * @param {string} dataType type of data
 * @return {Array} new array
 */

function filterBy(arr, dataType) {
    return arr.filter((item) => typeof item !== dataType);
}

const someArr = [
    3,
    4,
    "red",
    null,
    "blue",
    true, [1, 2, 3],
    { key: 1, key2: 4 },
];

console.log(filterBy(someArr, "number"));