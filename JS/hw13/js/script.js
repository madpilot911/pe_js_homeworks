const colorTheme = {
    btnContainer: document.createElement("div"),
    btnDark: document.createElement("button"),
    bigDaddy: document.body,
    moon: `<i class="far fa-moon"></i>`,
    sun: `<i class="fas fa-sun"></i>`,
    changeColor: function() {
        localStorage.getItem("color") ?
            this.bigDaddy.classList.add("dark") :
            this.bigDaddy.classList.remove("dark");
    },
    setBtnIcon: function() {
        this.btnDark.innerHTML = localStorage.getItem("color") ?
            this.sun :
            this.moon;
    },
    initBtn: function() {
        this.btnContainer.classList.add("btn-container");
        document.querySelector(".head-section").append(this.btnContainer);
        this.btnDark.classList.add("btn-dark-light");
        colorTheme.setBtnIcon();
        colorTheme.changeColor();
        this.btnContainer.append(this.btnDark);
        return this.btnDark;
    },
    btnHandler: function(e) {
        localStorage.getItem("color") ?
            e.currentTarget.classList.add("btn-dark") :
            e.currentTarget.classList.remove("btn-dark");
        if (!document.querySelector(".dark")) {
            localStorage.setItem("color", true);
        } else {
            localStorage.removeItem("color");
        }
        colorTheme.setBtnIcon();
        colorTheme.changeColor();
    },
};

const btnDark = colorTheme.initBtn();
btnDark.addEventListener("click", colorTheme.btnHandler);