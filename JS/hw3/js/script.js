let number1 = +prompt("Enter number 1", 0);
let number2 = +prompt("Enter number 2", 0);
let operator = prompt("Enter Math action", "+");

while (isNaN(+number1)) {
    number1 = +prompt("Please enter NUMBER 1", number1);
    number2 = +prompt("Please enter NUMBER 2", number2);
}

while (isNaN(+number2)) {
    number1 = +prompt("Please enter NUMBER 1", number1);
    number2 = +prompt("Please enter NUMBER 2", number2);
}

function calc(number1, number2, operator) {
    let result;
    switch (operator) {
        case "+":
            result = number1 + number2;
            break;

        case "-":
            result = number1 - number2;
            break;

        case "*":
            result = number1 * number2;
            break;

        case "/":
            result = number1 / number2;
            break;
    }
    return result;
}

let result = calc(number1, number2, operator);
console.log(result);