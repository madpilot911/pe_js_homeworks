const images = document.querySelectorAll(".image-to-show");
let delay = 10000;
let showImages = launch();
let counter = delay / 1000;
let timerOn;

function launch() {
    return setInterval(toggleClass, delay, images, "active");
}

function createTimer() {
    const timer = document.createElement("div");
    timer.classList.add("timer");
    document.querySelector(".service-wrapper").append(timer);

    return timer;
}

function makeTimerGreatAgain() {
    const timer = document.querySelector(".timer");
    if (!timer.classList.contains("stopped")) {
        timer.innerHTML = counter;
        if (counter < 4) {
            timer.style.color = "red";
        } else {
            timer.style.color = "#3a3a3a";
        }
        if (counter === 0) {
            timer.innerHTML = "";
            counter = delay / 1000;

            return;
        }
        counter--;

        setTimeout(makeTimerGreatAgain, 1000);
    } else {
        counter = delay / 1000;
        timer.innerHTML = "Paused";
        timer.style.color = "#3a3a3a";
    }
}

function toggleClass(elements, cssClass) {
    makeTimerGreatAgain();

    for (let i = 0; i < elements.length; ++i) {
        if (elements[i].classList.contains(cssClass)) {
            elements[i].classList.remove(cssClass);
            if (i + 1 === elements.length) {
                i = 0;
                elements[0].classList.add(cssClass);
            } else {
                elements[i + 1].classList.add(cssClass);
            }

            break;
        }
    }
}

function createStopBtn(appendNode) {
    const stopBtn = document.createElement("button");
    stopBtn.classList.add("button");
    stopBtn.insertAdjacentHTML("afterbegin", "Прекратить");
    stopBtn.addEventListener("click", () => {
        clearInterval(showImages);
        document.querySelector(".timer").classList.add("stopped");
    });
    appendNode.append(stopBtn);
}

function createContinueBtn(appendNode) {
    const continueBtn = document.createElement("button");
    continueBtn.classList.add("button");
    continueBtn.insertAdjacentHTML("afterbegin", "Возобновить");
    continueBtn.addEventListener("click", () => {
        document.querySelector(".timer").classList.remove("stopped");
        toggleClass(images, "active");
        showImages = launch();
    });
    appendNode.append(continueBtn);
}

createStopBtn(document.querySelector(".service-wrapper"));
createContinueBtn(document.querySelector(".service-wrapper"));
createTimer();
toggleClass(images, "active");