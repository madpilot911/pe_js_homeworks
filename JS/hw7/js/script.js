const someArr = [
    "Kiev",
    { city1: "Kryvyi Rih", city2: "Dnipro" },
    "Kharkiv",
    "Odessa",
    "Lviv",
];

// Задание одной функцией
// function createList(arr) {
//     const list = document.createElement("ul");
//     const arrOfTags = arr.map((el) => {
//         if (typeof el !== "object") {
//             return `<li>${el}</li>`;
//         } else {
//             const nestedList = [];
//             for (const item in el) {
//                 nestedList.push(`<li>${el[item]}</li>`);
//             }
//             return `<ul>${nestedList.join("")}</ul>`;
//         }
//     });
//     list.innerHTML = arrOfTags.join("");
//     document.body.append(list);
//     setTimeout(() => list.remove(), 10000);
// }

// createList(someArr);

/**
 * Create HTML list from Array
 * @param {Array} arr Array with list content
 * @param {String} tag "ul" or "ol" to create different types of list
 */
function createList(arr, tag) {
    const list = document.createElement(tag);
    const arrOfTags = arr.map((el) => {
        if (typeof el !== "object") {
            return `<li>${el}</li>`;
        } else {
            const nestedList = [];
            for (const item in el) {
                nestedList.push(`<li>${el[item]}</li>`);
            }
            return `<ul>${nestedList.join("")}</ul>`;
        }
    });
    list.innerHTML = arrOfTags.join("");
    return list;
}

/**
 * Remove element from page
 * @param {Node} element Element to be removed
 * @param {Number} time Timeout
 */
function deleteElement(element, time) {
    setTimeout(() => element.remove(), time);
}

let list = createList(someArr, "ol");
document.body.append(list);

deleteElement(list, 10000);

//Таймер одной функцией
// function createCountdown(count) {
//     const timer = document.createElement("div");
//     let dt = new Date();
//     let dt2 = dt.getSeconds() + count;
//     let seconds;
//     timer.innerHTML = count;
//     let interval = setInterval(() => {
//         dt = new Date();
//         seconds = dt2 - dt.getSeconds();
//         timer.innerHTML = seconds;
//     }, 1000);
//     document.body.prepend(timer);
//     setTimeout(() => {
//         clearInterval(interval);
//         timer.remove();
//     }, count * 1000);
// }

// createCountdown(10);

function createCountdown(count) {
    const timer = document.createElement("div");
    let dt = new Date();
    let dt2 = dt.getSeconds() + count;
    let seconds;
    timer.innerHTML = count;
    let interval = setInterval(() => {
        dt = new Date();
        seconds = dt2 - dt.getSeconds();
        timer.innerHTML = seconds;
    }, 1000);
    setTimeout(() => {
        clearInterval(interval);
        timer.remove();
    }, count * 1000);
    return timer;
}

let countdown = createCountdown(10);
document.body.prepend(countdown);