const buttons = document.querySelectorAll(".btn");

document.addEventListener("keypress", (e) => {
    buttons.forEach((button) => {
        button.classList.remove("pressed");
        if (e.key.charAt(0).toUpperCase() + e.key.slice(1) === button.innerHTML) {
            button.classList.add("pressed");
        }
    });
});