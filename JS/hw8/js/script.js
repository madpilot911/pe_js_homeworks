const price = document.getElementById("price");

function createPriceSpan(e) {
    const result = createHTMLelement("span", e.target.value, "active");
    const container = document.querySelector(".selected-price");
    container.append(result);
    return result;
}

function createWarningSpan(node, method) {
    const warning = document.createElement("span");
    warning.setAttribute("id", "warning");
    warning.insertAdjacentHTML("afterbegin", "Please enter correct price");
    node[method](warning);
    return warning;
}

function createButton(node, method) {
    const btn = createHTMLelement("span", "", "button");
    node[method](btn);
    btn.addEventListener("click", (e) => {
        e.target.remove();
        node.remove();
        price.value = "";
    });
}

function setClass(element, method, classes) {
    if (typeof classes !== "string") {
        for (let i = 0; i < classes.length; i++) {
            element.classList[method](classes[i]);
        }
    } else {
        element.classList[method](classes);
    }
}

price.addEventListener("blur", function(e) {
    if (e.target.value > 0) {
        const span = createPriceSpan(e);
        createButton(span, "after");
        setClass(price, "add", "correct");
        const warningSpan = document.querySelector(".warning");
        setClass(price, "remove", "invalid");
        if (warningSpan) {
            warningSpan.remove();
        }
    } else if (!document.getElementById("warning")) {
        const container = document.getElementById("container");
        const warning = createWarningSpan(container, "append");
        setClass(warning, "add", "warning");
        setClass(price, "add", "invalid");
    }
});

function createHTMLelement(tag, text, className) {
    const element = document.createElement(tag);
    element.insertAdjacentHTML("afterbegin", text);
    element.classList.add(className);
    return element;
}