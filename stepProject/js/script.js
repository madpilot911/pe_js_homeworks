document.addEventListener("DOMContentLoaded", onLoadReady);

function onLoadReady() {
    // Our Service
    ourServiceData.tabs.addEventListener("click", ourServiceData.activateTab);
    ourServiceData.tabs.addEventListener("click", ourServiceData.activateContent);
    // Our Work
    ourWorkData.tabs.addEventListener("click", ourWorkData.activateTab);
    ourWorkData.tabs.addEventListener("click", ourWorkData.loadImages);
    ourWorkData.tabsContent.addEventListener("click", turnTheCard);
    ourWorkData.loadRandomImages(ourWorkData.maxImageToShow);
    // ourWorkData.onHover();
    // Testimonial
    showQuote();
    people.addEventListener("click", youAreChosen);
    people.addEventListener("click", showQuote);
    activateRightButton();
    activateLeftButton();
}