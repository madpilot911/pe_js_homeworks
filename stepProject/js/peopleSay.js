const people = document.querySelector(".people");
let persons = document.querySelectorAll(".people div");

const peopleData = [{
        comment: "The broccoli says 'I look like a small tree', the mushroom says 'I look like an umbrella', and the banana says 'Can we please change the subject?'",
        name: "John Grinch",
        position: "Truck Driver",
        avatar: "./images/people/person1.png",
    },
    {
        comment: "I’m not insane. My mother had me tested",
        name: "Rick Blackwhite",
        position: "Big Dude",
        avatar: "./images/people/person2.png",
    },
    {
        comment: "Two things are infinite: the universe and human stupidity; and I'm not sure about the universe",
        name: "Kevin Trutsi",
        position: "Mustashe Dude",
        avatar: "./images/people/person3.png",
    },
    {
        comment: "My mother always used to say: The older you get, the better you get, unless you’re a banana",
        name: "Jack Jackson",
        position: "Strange Dude",
        avatar: "./images/people/person4.png",
    },
];

const testimonialTemplate = {
    comment: document.querySelector(".comment"),
    name: document.querySelector(".name"),
    position: document.querySelector(".position"),
    avatar: document.querySelector(".avatar"),
};

function youAreChosen(e) {
    for (let i = 0; i < persons.length; i++) {
        persons[i].classList.remove("chosen");
    }
    e.target.classList.add("chosen");
}

function showQuote() {
    for (let i = 0; i < persons.length; i++) {
        if (persons[i].classList.contains("chosen")) {
            testimonialTemplate.comment.innerHTML = `${peopleData[i].comment}`;
            testimonialTemplate.name.innerHTML = `${peopleData[i].name}`;
            testimonialTemplate.position.innerHTML = `${peopleData[i].position}`;
            testimonialTemplate.avatar.innerHTML = `<img src="${peopleData[i].avatar}"></img>`;
        }
    }
}

function activateRightButton() {
    const rightBtn = document.querySelector(".right");
    rightBtn.addEventListener("click", () => {
        for (let i = 0; i < persons.length; i++) {
            if (persons[i].classList.contains("chosen") && i !== persons.length - 1) {
                persons[i].classList.remove("chosen");
                persons[i + 1].classList.add("chosen");
                showQuote();
                return;
            } else if (
                persons[i].classList.contains("chosen") &&
                i === persons.length - 1
            ) {
                persons[i].classList.remove("chosen");
                persons[0].classList.add("chosen");
                showQuote();
                return;
            }
        }
    });
}

function activateLeftButton() {
    const leftBtn = document.querySelector(".left");
    leftBtn.addEventListener("click", () => {
        for (let i = 0; i < persons.length; i++) {
            if (persons[i].classList.contains("chosen") && i !== 0) {
                persons[i].classList.remove("chosen");
                persons[i - 1].classList.add("chosen");
                showQuote();
                return;
            } else if (persons[i].classList.contains("chosen") && i === 0) {
                persons[i].classList.remove("chosen");
                persons[persons.length - 1].classList.add("chosen");
                showQuote();
                return;
            }
        }
    });
}