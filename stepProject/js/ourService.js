const ourServiceData = {
    tabs: document.querySelector(".tabs"),
    tabsTitle: document.querySelectorAll(".tabs .tabs-title"),
    tabsContent: document.querySelector(".tabs-content").children,
    activateTab: function(e) {
        ourServiceData.tabsTitle.forEach((element) =>
            element.classList.remove("active")
        );
        e.target.classList.add("active");
    },
    activateContent: function() {
        for (let i = 0; i < ourServiceData.tabsContent.length; i++) {
            ourServiceData.tabsContent[i].dataset.hidden = "true";
        }
        for (let i = 0; i < ourServiceData.tabsTitle.length; i++) {
            if (ourServiceData.tabsTitle[i].classList.contains("active")) {
                ourServiceData.tabsContent[i].dataset.hidden = "false";
            }
        }
    },
};

const images = document.querySelectorAll(".service-image");

for (let i = 0; i < images.length; i++) {
    images[i].style.backgroundImage = `url(../images/services/service${
    i + 1
  }.jpg)`;
}