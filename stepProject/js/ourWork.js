// При загрузке изображений с сервера и медленном интернете этот код приведет к коллапсу

const ourWorkData = {
    maxImageToShow: 12,
    tabs: document.querySelector(".our-work-nav"),
    tabsTitle: document.querySelectorAll(".work-title"),
    tabsContent: document.querySelector(".our-work-container"),
    arrayOfId: function() {
        return Array.from(ourWorkData.tabsTitle).map((element) =>
            element.getAttribute("id")
        );
    },
    randomId: function() {
        return Math.floor(Math.random() * (ourWorkData.tabsTitle.length - 1));
    },
    randomIterator: function() {
        return Math.floor(Math.random() * (ourWorkData.maxImageToShow - 1) + 1);
    },
    activateTab: function(e) {
        ourWorkData.tabsTitle.forEach((element) => {
            element.classList.remove("active");
        });
        e.target.classList.add("active");
        loadBtn.classList.remove("inactive-btn");
        ourWorkData.clickCounter = 0;
    },

    createImage: function(id, iterator) {
        let ourWorkImg = document.createElement("div");
        ourWorkImg.classList.add("our-work-img-front", "image");

        const img = document.createElement("img");
        img.setAttribute("src", `../images/${id}/${id}${iterator}.jpg`);
        let backImg = document.createElement("div");

        backImg.innerHTML = `<h3>creative design</h3><p>${id
      .split("-")
      .join(" ")}</p>`;
        backImg.classList.add("our-work-img-back");
        img.addEventListener("error", function() {
            ourWorkImg.classList.add("failed");
        });
        img.addEventListener("load", function() {
            ourWorkImg.append(backImg);
            ourWorkImg.append(img);
        });

        return ourWorkImg;
    },
    clearContent: function() {
        ourWorkData.tabsContent.innerHTML = "";
    },
    loadImages: function() {
        for (let i = 0; i < ourWorkData.tabsTitle.length; i++) {
            if (ourWorkData.tabsTitle[i].classList.contains("active")) {
                let id = ourWorkData.tabsTitle[i].getAttribute("id");
                ourWorkData.clearContent();
                if (id !== "all") {
                    for (let i = 1; i <= ourWorkData.maxImageToShow; i++) {
                        let image = ourWorkData.createImage(id, i);

                        ourWorkData.tabsContent.append(image);
                    }
                } else {
                    ourWorkData.loadRandomImages(ourWorkData.maxImageToShow);
                }
            }
        }
    },
    loadRandomImages: function(imageQuantity) {
        const arrayOfId = ourWorkData.arrayOfId();
        arrayOfId.splice(0, 1);

        for (let i = 1; i <= imageQuantity; i++) {
            let image = ourWorkData.createImage(
                arrayOfId[ourWorkData.randomId()],
                ourWorkData.randomIterator()
            );

            ourWorkData.tabsContent.append(image);
        }
        checkImageQuantity(imageQuantity);
    },
    clickCounter: 0,
};

function checkImageQuantity(quantity) {
    const checker = quantity;

    setTimeout(function() {
        const countDivs = function() {
            return ourWorkData.tabsContent.querySelectorAll(".image").length;
        };
        const createdDivs = countDivs();

        const nonLoadedImages = ourWorkData.tabsContent.querySelectorAll(".failed");

        if (
            nonLoadedImages.length &&
            createdDivs - nonLoadedImages.length < checker
        ) {
            for (let i = 0; i < nonLoadedImages.length; i++) {
                nonLoadedImages[i].remove();
            }
            const loadedImages = countDivs();

            if (loadedImages < checker) {
                ourWorkData.loadRandomImages(checker - loadedImages);
            }
            checkImageQuantity(checker);
        }
    }, 600);
}

const loadBtn = document.querySelector(".load");

loadBtn.addEventListener("click", function() {
    ourWorkData.clickCounter++;
    if (ourWorkData.clickCounter < 3) {
        const createdImages = ourWorkData.tabsContent.querySelectorAll(".image");

        for (let i = 0; i < createdImages.length; i++) {
            createdImages[i].classList.remove("image");
        }
        ourWorkData.loadRandomImages(ourWorkData.maxImageToShow);
        if (ourWorkData.clickCounter === 2) {
            loadBtn.classList.add("inactive-btn");
        }
    }
});

function turnTheCard(e) {
    const images = e.currentTarget.children;
    if (e.target.parentElement.classList.contains("clicked")) {
        e.target.parentElement.classList.toggle("clicked");
    } else {
        for (let i = 0; i < images.length; i++) {
            images[i].classList.remove("clicked");
            e.target.parentElement.classList.add("clicked");
        }
    }
}