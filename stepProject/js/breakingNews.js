//тут не до конца сформированы оьъекты постов

const posts = [{
        title: "post number 1",
        autor: "Linda",
        comments: 2,
    },

    {
        title: "post number 2",
        autor: "Natasha",
        comments: 5,
    },

    {
        title: "post number 3",
        autor: "Andrew",
        comments: 1,
    },

    {
        title: "post number 4",
        autor: "Angry Dude",
        comments: 17,
    },

    {
        title: "post number 5",
        autor: "Stupid Dude",
        comments: 1.5,
    },

    {
        title: "post number 6",
        autor: "Mike",
        comments: 2,
    },

    {
        title: "post number 7",
        autor: "John",
        comments: 1,
    },

    {
        title: "post number 8",
        autor: "Audrey",
        comments: 3,
    },

    {
        title: "post number 1",
        autor: "Linda",
    },
];

const postImages = document.querySelectorAll(".post-image");

for (let i = 0; i < postImages.length; i++) {
    postImages[i].style.backgroundImage = `url(../images/news/news${i + 1}.png)`;
}

const dateOfPost = function() {
    const date = new Date();
    const day = date.getDate();
    const month = date.getMonth();
    let result;
    switch (month) {
        case 0:
            result = "Jan";
            break;
        case 1:
            result = "Feb";
            break;
        case 2:
            result = "Mar";
            break;
        case 3:
            result = "Apr";
            break;
        case 4:
            result = "May";
            break;
        case 5:
            result = "Jun";
            break;
        case 6:
            result = "Jul";
            break;
        case 7:
            result = "Aug";
            break;
        case 8:
            result = "Sep";
            break;
        case 9:
            result = "Oct";
            break;
        case 10:
            result = "Nov";
            break;
        case 11:
            result = "Dec";
            break;
    }
    return `<span>${day}</span><span>${result}</span>`;
};

const dates = document.querySelectorAll(".date");
for (let i = 0; i < dates.length; i++) {
    dates[i].innerHTML = dateOfPost();
}

const postTitles = document.querySelectorAll(".post h3");
for (let i = 0; i < postTitles.length; i++) {
    postTitles[i].innerHTML = `${posts[i].title}`;
}

const autors = document.querySelectorAll(".autor");
for (let i = 0; i < postTitles.length; i++) {
    autors[i].innerHTML = `by ${posts[i].autor}`;
}

const comments = document.querySelectorAll(".comments");
for (let i = 0; i < postTitles.length; i++) {
    comments[i].innerHTML = `comments ${posts[i].comments}`;
}